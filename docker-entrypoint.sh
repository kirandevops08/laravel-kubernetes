#!/bin/bash

cp /var/www/tmp/.env.example /var/www/tmp/.env

cp -R /var/www/tmp/. /var/www/html/
chown -R www-data:www-data /var/www/html

cd /var/www/html/ && php artisan cache:clear && php artisan config:clear && composer du 
exec "$@"
